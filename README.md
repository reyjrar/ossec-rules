This is the Atomicorp branch for OSSEC-HIDS rules. It is based on a rules.d/ and decoders.d/ layout. 

decoder.xml has been deprecated, all new decoders live in independent files.

Rules are loaded in numerical order, allowing for easier organization of dependencies across rules.
